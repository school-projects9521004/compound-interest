public class Main {
    public static void main(String[] args) {
        double incomeTarget = 40000;
        double interestRate = .08;
        double contribution = 200;
        double startingAmount = 1000;

        // Create an instance of the Main class to call the getYears method
        Main compoundInterestCalculator = new Main();

        // Call the getYears method to calculate the number of years
        double years = compoundInterestCalculator.getYears(incomeTarget, interestRate, contribution, startingAmount);

        System.out.println("Number of Years Required: " + years);
    }

    public int getYears(double incomeTarget, double interestRate, double contribution, double startingAmount) {
        // Perform the compound interest calculation
        double currentAmount = startingAmount;
        int years = 0;

        while (currentAmount*interestRate < incomeTarget) {
            currentAmount = (currentAmount + contribution) * (1.0+interestRate);
            years++;
        }

        return years;
    }
}
