# Compound Interest Calculator

## Description

This Java program is designed to calculate the number of years it takes to reach a target amount of money per year, considering a specified interest rate, monthly contribution, starting amount, and compound frequency.

### Features

1. **Target Money Per Year:**
   - Input the desired annual amount you want to achieve.

2. **Interest Rate:**
   - Enter the annual interest rate for your investment.

3. **Monthly Contribution:**
   - Specify the amount you plan to invest monthly.

4. **Starting Amount:**
   - Input the initial amount you have for the investment.

5. **Compound Frequency:**
   - Choose how often the interest is compounded (e.g., annually, semi-annually, quarterly, or monthly).

### Usage

1. **Run the Program:**
   - Execute the Java program.

2. **Input Details:**
   - Follow the prompts to enter the target money per year, interest rate, monthly contribution, starting amount, and compound frequency.

3. **View Results:**
   - The program will calculate and display the number of years required to achieve the target amount.

### Example

```java

